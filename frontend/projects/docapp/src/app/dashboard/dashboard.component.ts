import { Component, OnInit } from "@angular/core";
import { GlobalService } from "../global.service";
import { DataService } from "../../../../../src/app/data.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  private userData;
  upcomingBookings;
  create = false;

  days = [
    { name: "Mon", check: false },
    { name: "Tue", check: false },
    { name: "Wed", check: false },
    { name: "Thur", check: false },
    { name: "Fri", check: false },
    { name: "Sat", check: false },
    { name: "Sun", check: false }
  ];

  slots = [];

  hours;
  time;

  constructor(
    private service: GlobalService,
    private userService: DataService
  ) {
    const myUserLocal = localStorage.getItem("doctor_patiend_loggedin_user");
    try {
      const localData = JSON.parse(myUserLocal);
      if (localData && "data" in localData) {
        this.userData = localData["data"];
      } else {
        this.userData = {};
      }
    } catch (error) {
      this.userData = {};
    }
  }

  ngOnInit() {
    if (this.userService.checkAuthOrRedirect("doctor")) {
      this.service.fetchMyUpcomingBookings(this.userData.id).subscribe(
        res => {
          this.upcomingBookings = res;
        },
        err => {
          console.log(err);
        }
      );
    }
  }

  createSlots() {
    const days = this.days.filter(o => o.check === true).map(o => o.name);

    const data = {
      availabilityDays: days,
      availabilityHours: this.hours,
      userId: this.userData.id,
      availabilityTime: this.time
    };
    this.service.createDoctorSlots(data).subscribe(
      res => {
        alert("Slot Created");
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }
}
