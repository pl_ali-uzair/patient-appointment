import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../environments/environment";

@Injectable({
  providedIn: "root"
})
export class GlobalService {
  constructor(private http: HttpClient) {}

  createDoctorSlots(params: any) {
    return this.http.post(`${environment.api_url}/doctor/setSlots`, params);
  }

  fetchMyUpcomingBookings(doctorId: any) {
    return this.http.get(`${environment.api_url}/doctor/myUpcomingBookings/${doctorId}`);
  }

  getDoctorSlots() {
    return this.http.get(`${environment.api_url}/doctor/myslots`);
  }
}
