import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { DataService, Errors } from "../data.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  errors: Errors = new Errors();
  authForm: FormGroup;
  isSubmitting = false;

  constructor(
    private fb: FormBuilder,
    private userService: DataService
  ) {
    // use FormBuilder to create a form group
    this.authForm = this.fb.group({
      name: ["", Validators.required],
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.userService.checkAuthOrRedirect("login/register");
  }

  register() {
    this.isSubmitting = true;
    this.errors = new Errors();

    const credentials = this.authForm.value;
    this.userService.attemptAuth('register', credentials).subscribe(
      data => {
        this.userService.checkAuthOrRedirect("login/register");
      },
      err => {
        this.isSubmitting = false;
        const errorMessage = "message" in err ? err.message : err;
        alert(errorMessage);
        this.errors = err;
      }
    );
  }
}
