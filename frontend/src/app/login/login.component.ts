import { Component, OnInit, Input } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { DataService, Errors } from "../data.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  errors: Errors = new Errors();
  authForm: FormGroup;
  isSubmitting = false;

  constructor(
    private fb: FormBuilder,
    private userService: DataService
  ) {
    // use FormBuilder to create a form group
    this.authForm = this.fb.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.userService.checkAuthOrRedirect("login/register");
  }

  login() {
    this.isSubmitting = true;
    this.errors = new Errors();

    const credentials = this.authForm.value;
    this.userService.attemptAuth("login", credentials).subscribe(
      data => {
        this.userService.checkAuthOrRedirect("login/register");
      },
      err => {
        this.isSubmitting = false;
        const errorMessage = "message" in err ? err.message : err;
        alert(errorMessage);
        this.errors = err;
      }
    );
  }
}
