const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
var morgan = require('morgan')

// routes
const apiRoutes = require("./routes/default");
const userRoutes = require("./routes/user");
const adminRoutes = require("./routes/admin");
const doctorRoutes = require("./routes/doctor");
const patientRoutes = require("./routes/patient");

// db connection
mongoose
  .connect(
    "mongodb://shahzaib.imran:purelogics94@ds149344.mlab.com:49344/docdb",
    { useNewUrlParser: true }
  )
  .then(() => console.log("DB Connected"), err => console.log(err));

// middleware configuration
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'))

// api endpoints
app.use("/api/user", userRoutes);
app.use("/api/admin", adminRoutes);
app.use("/api/doctor", doctorRoutes);
app.use("/api/patient", patientRoutes);
app.use("/api", apiRoutes);

// server port binding
app.listen(3000, () => {
  console.log("Listening on port 3000");
});
