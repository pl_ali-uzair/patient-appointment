const jwt = require("jsonwebtoken");

const verifyToken = function(req, res, next) {
  jwt.verify(req.headers["authorization"], "secret", function(err, decoded) {
    if (err || !"_id" in decoded) {
      return res.status(400).json({ message: "Invalid Token!" });
    } else {
      req.user = decoded;
      next();
    }
  });
};

module.exports = { verifyToken };
