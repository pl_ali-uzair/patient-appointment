const express = require("express");
const router = express.Router();
const User = require("../models/users");
const bcrypt = require("bcryptjs");

const corsHelper = require("../helpers/cors");
router.use(corsHelper.configureCors);

router.get("/", (req, res) => {
  res.json({ api: "user" });
});

router.delete("/delete/:id/:role", (req, res) => {
  if (+req.params.role === 1) {
    User.findOneAndDelete({ _id: req.params.id }, (err, data) => {
      if (err) {
        res.status(403).json({ message: "Invalid id Provided!" });
      } else {
        res.status(200).json(data);
      }
    });
  } else {
    res.status(403).json({ message: "You are not an Admin!" });
  }
});

router.get("/allusers", (req, res) => {
  User.find({}, { password: 0 }, (err, data) => {
    if (err) {
      res.status(400).json({ message: "Some Error" });
    } else {
      res.status(200).json(data);
    }
  });
});

// get data of single user to edit it
router.get("/editUser/:userId", (req, res) => {
  User.findOne({ _id: req.params.userId }, { password: 0 }, (err, data) => {
    if (err) {
      res.status(400).json({ message: "Some Error" });
    } else {
      res.status(200).json(data);
    }
  });
});

// update single user
router.put("/updateUser/:userId", (req, res) => {
  User.findOne({ _id: req.params.userId }, (err, data) => {
    if (err) return res.status(400).json({ message: "Some Error" });
    if (!data) return res.status(404).json({ message: "User not found" });
    data.name = req.body.name;
    data.username = req.body.username;
    if (req.body.password)
      data.password = bcrypt.hashSync(req.body.password, 10);
    data.save((err, res) => {
      if (err) {
        res.status(400).json({ message: "User not updated!", err: err });
      } else {
        delete data.password;
        res
          .status(200)
          .json({ message: "User updated successfully.", user: data });
      }
    });
  });
});

module.exports = router;
