const express = require("express");
const router = express.Router();

const corsHelper = require("../helpers/cors");
router.use(corsHelper.configureCors);

router.get("/", (req, res) => {
  res.json({ api: "admin" });
});

module.exports = router;
