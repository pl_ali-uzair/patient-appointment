const express = require("express");
const router = express.Router();
const moment = require("moment");
const asyncLib = require("async");
const authHelper = require("../helpers/auth");

const Booking = require("../models/bookings");

const corsHelper = require("../helpers/cors");
router.use(corsHelper.configureCors);

router.get("/", (req, res) => {
  res.json({ api: "patient" });
});

router.post("/new-appointment", authHelper.verifyToken, (req, res) => {
  req.body.patientId = req.user._id;
  let new_booking = new Booking(req.body);
  new_booking.save((err, data) => {
    if (err) {
      res.status(400).json({ message: "Appointment was not created!", err });
    } else {
      res.status(200).json({ message: "Appointment submitted!", slots: data });
    }
  });
});

router.get("/myAppointments", authHelper.verifyToken, (req, res) => {
  asyncLib.parallel(
    {
      upcoming: function(callback) {
        Booking.find(
          { patientId: req.user._id, availabilityDate: { $gt: new Date() } },
          (err, data) => {
            // format date
            data.forEach((item, index) => {
              data[index].availabilityDateFormatted = moment(
                item.availabilityDate
              ).format("LL");
            });
            callback(err, data);
          }
        )
          .sort("availabilityDate")
          .populate("doctorId", "username");
      },
      previous: function(callback) {
        Booking.find(
          { patientId: req.user._id, availabilityDate: { $lt: new Date() } },
          (err, data) => {
            // format date
            data.forEach((item, index) => {
              data[index].availabilityDateFormatted = moment(
                item.availabilityDate
              ).format("LL");
            });
            callback(err, data);
          }
        )
          .sort("availabilityDate")
          .populate("doctorId", "username");
      }
    },
    function(err, result) {
      if (err) {
        res.status(403).json({ message: "Unable to fetch appointments!" });
      } else {
        res.status(200).json(result);
      }
    }
  );
});
module.exports = router;
