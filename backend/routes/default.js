const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const User = require("../models/users");

const corsHelper = require("../helpers/cors");
router.use(corsHelper.configureCors);

router.get("/", (req, res) => {
  res.json({ api: "default" });
});

router.post("/signup", (req, res) => {
  let reqData = req.body;
  reqData.password = bcrypt.hashSync(req.body.password, 10);

  let user = new User(reqData);
  if (!user.role) {
    // role will be number and for patient it is 2
    user.role = 2;
  }

  user.save((err, data) => {
    if (err) {
      res.status(400).json({ message: "User not created!", err: err });
    } else {
      jwt.sign(data.toJSON(), "secret", function(err, token) {
        if (err) {
          res.status(400).json("Token not generated");
        }
        let userdata = {
          name: data.name,
          username: data.username,
          email: data.email,
          id: data._id,
          role: data.role
        };
        res
          .status(200)
          .json({ message: "User Created!", data: userdata, token: token });
      });
    }
  });
});

router.post("/login", (req, res) => {
  User.findOne(
    { $or: [{ username: req.body.username }, { username: req.body.email }] },
    (err, data) => {
      if (err) {
        res.status(400).json({ message: "Error while finding User!", err });
      }
      if (!data) {
        res.status(400).json({ message: "User not found!" });
      } else {
        let isAuthenticated = bcrypt.compareSync(
          req.body.password,
          data.password
        );

        if (isAuthenticated) {
          jwt.sign(data.toJSON(), "secret", function(err, token) {
            if (err) {
              res.status(400).json("Token not generated");
            }
            let userdata = {
              name: data.name,
              username: data.username,
              email: data.email,
              id: data._id,
              role: data.role
            };
            res
              .status(200)
              .json({ message: "LoggedIn", data: userdata, token: token });
          });
        } else {
          res.status(400).json({ message: "Incorrect Password" });
        }
      }
    }
  );
});

module.exports = router;
