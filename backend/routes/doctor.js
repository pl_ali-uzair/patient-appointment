const express = require("express");
const router = express.Router();
const moment = require("moment");

const Doctor = require("../models/doctor");
const User = require("../models/users");
const Booking = require("../models/bookings");

const corsHelper = require("../helpers/cors");
router.use(corsHelper.configureCors);

router.get("/", (req, res) => {
  res.json({ api: "doctor" });
});

router.post("/setSlots", (req, res) => {
  let doc = new Doctor(req.body);

  doc.save((err, data) => {
    if (err) {
      res.status(400).json({ message: "Slots were not created!", err });
    } else {
      res.status(200).json({ message: "Slots Created!", slots: data });
    }
  });
});

router.get("/slots", (req, res) => {
  Doctor.find({}, (err, data) => {
    if (err) {
      res.status(403).json({ message: "Invalid id Provided!" });
    } else {
      res.status(200).json(data);
    }
  });
});

router.get("/myUpcomingBookings/:doctorId", (req, res) => {
  Booking.find(
    { doctorId: req.params.doctorId, availabilityDate: { $gt: new Date() } },
    (err, data) => {
      if (err) {
        return res.status(400).json({ message: "Can't fetch bookings." });
      }
      if (data) {
        // format date
        data.forEach((item, index) => {
          data[index].availabilityDateFormatted = moment(
            item.availabilityDate
          ).format("LL");
        });
      }
      res.status(200).json(data);
    }
  )
    .sort("availabilityDate")
    .populate("patientId", "username");
});

router.get("/doctor-slots/:doctorId", (req, res) => {
  // Assumed Flow
  //     - Get 5 records of slots
  //     - Split records based on week day
  //     - Generate Date for every slot
  //     - Sort by date
  //     - Pick 5 slots

  let slots = [];
  Doctor.find({ userId: req.params.doctorId }, (err, data) => {
    if (err) {
      res.status(403).json({ message: "Invalid id Provided!" });
    } else {
      // loop through records
      data.forEach(row => {
        // each row can have multiple days. So split row by days
        row.availabilityDays.forEach(slotDay => {
          slots.push({
            availabilityDay: slotDay,
            availabilityTime: row.availabilityTime,
            availabilityHours: row.availabilityHours,
            userId: row.userId
          });
        });
      });
      // Generate date for every slot
      slots.forEach((slot, index) => {
        const today = moment().day();
        const daysNumber = {
          Mon: 1,
          Tue: 2,
          Wed: 3,
          Thur: 4,
          Fri: 5,
          Sat: 6,
          Sun: 7
        };
        let slotDay;
        switch (slot.availabilityDay) {
          case "Mon":
            slotDay = daysNumber.Mon;
            if (today >= slotDay) slotDay = 7 + daysNumber.Mon;
            slots[index].availabilityDate = moment().day(slotDay);
            break;
          case "Tue":
            slotDay = daysNumber.Tue;
            if (today >= slotDay) slotDay = 7 + daysNumber.Tue;
            slots[index].availabilityDate = moment().day(slotDay);
            break;
          case "Wed":
            slotDay = daysNumber.Wed;
            if (today >= slotDay) slotDay = 7 + daysNumber.Wed;
            slots[index].availabilityDate = moment().day(slotDay);
            break;
          case "Thur":
            slotDay = daysNumber.Thur;
            if (today >= slotDay) slotDay = 7 + daysNumber.Thur;
            slots[index].availabilityDate = moment().day(slotDay);
            break;
          case "Fri":
            slotDay = daysNumber.Fri;
            if (today >= slotDay) slotDay = 7 + daysNumber.Fri;
            slots[index].availabilityDate = moment().day(slotDay);
            break;
          case "Sat":
            slotDay = daysNumber.Sat;
            if (today >= slotDay) slotDay = 7 + daysNumber.Sat;
            slots[index].availabilityDate = moment().day(slotDay);
            break;
          case "Sun":
            slotDay = daysNumber.Sun;
            if (today >= slotDay) slotDay = 7 + daysNumber.Sun;
            slots[index].availabilityDate = moment().day(slotDay);
            break;
        }
        slots[index].availabilityDate.hour(slots[index].availabilityTime);
        // format date
        slots[index].availabilityDateFormatted = moment(
          slots[index].availabilityDate
        ).format("LL");
      });
      // sort by date
      slots.sort(function(a, b) {
        if (a.availabilityDate < b.availabilityDate) {
          return -1;
        }
        if (a.availabilityDate > b.availabilityDate) {
          return 1;
        }
        return 0;
      });
      // get only 5 slots
      slots = slots.slice(0, 5);
      res.status(200).json(slots);
    }
  }).limit(5);
});

router.get("/alldoctors", (req, res) => {
  User.find({ role: 3 }, { password: 0 }, (err, data) => {
    if (err) {
      res.status(400).json({ message: "Some Error" });
    } else {
      res.status(200).json(data);
    }
  });
});

// get bookings of all doctors
router.get("/viewBookings", (req, res) => {
  Booking.find({}, (err, data) => {
    // format date
    data.forEach((item, index) => {
      data[index].availabilityDateFormatted = moment(
        item.availabilityDate
      ).format("LL");
    });
    res.status(200).json(data);
  })
    .sort("availabilityDate")
    .populate("doctorId", "name")
    .populate("patientId", "name");
});

router.get("/editBooking/:bookingId", (req, res) => {
  Booking.findOne({ _id: req.params.bookingId }, (err, data) => {
    if (err) {
      res.status(400).json({ message: "Some Error" });
    } else {
      res.status(200).json(data);
    }
  })
    .populate("doctorId", "name")
    .populate("patientId", "name");
});

router.put("/updateBooking/:bookingId", (req, res) => {
  Booking.findOne({ _id: req.params.bookingId }, (err, data) => {
    if (err) return res.status(400).json({ message: "Some Error" });
    if (!data) return res.status(404).json({ message: "Booking not found" });
    data.availabilityDate = req.body.availabilityDate;
    data.availabilityDay = req.body.availabilityDay;
    data.availabilityTime = req.body.availabilityTime;

    data.save((err, res) => {
      if (err) {
        res.status(400).json({ message: "Booking not updated!", err: err });
      } else {
        delete data.password;
        res
          .status(200)
          .json({ message: "Booking updated successfully.", booking: data });
      }
    });
  });
});

// delete single booking
router.delete("/deleteBooking/:bookingId/:role", (req, res) => {
  if (+req.params.role === 1) {
    Booking.findOneAndDelete({ _id: req.params.bookingId }, (err, data) => {
      if (err) {
        res.status(403).json({ message: "Invalid id Provided!" });
      } else {
        res.status(200).json({ message: "Invalid id Provided!" });
      }
    });
  } else {
    res.status(403).json({ message: "Booking has been successfully." });
  }
});
module.exports = router;
